require('dotenv').config()
module.exports =
{
  "development": {
    "username": process.env.DB_USER,
    "password": process.env.DB_PASS,
    "database": "localhost",
    "host": "127.0.0.1",
    "dialect": "postgres"
  },
  "test": {
    "username": process.env.DB_USER,
    "password": process.env.DB_PASS,
    "database": "localhost",
    "host": "127.0.0.1",
    "dialect": "postgres"
  },
  "production": {
    "username": "mpnrqsteemitlk",
    "password": "6e2d36153c9a3d3138bcfff08aca8bf238e7b8f9d44ed20e3f8e00280058782a",
    "database": "d72j6gmk5kmu50",
    "host": "ec2-44-199-26-122.compute-1.amazonaws.com",
    "dialect": "postgres",
    "port": "5432",
    "dialectOptions": {
      "ssl": {
        "rejectUnauthorized": false
      }
    }
  }
}