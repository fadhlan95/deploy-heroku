'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
    await queryInterface.bulkInsert('Teachers', [
      {
        name: 'Idrus',
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        name: 'Fadhlan',
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        name: 'Fahmi',
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        name: 'Seiga',
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        name: 'Rayhan',
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        name: 'Rayan',
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        name: 'Wahid',
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        name: 'Rizky',
        createdAt: new Date(),
        updatedAt: new Date()
      },{
        name: 'Jihad',
        createdAt: new Date(),
        updatedAt: new Date()
      }
      ,{
        name: 'Vasya',
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        name: 'Joshua',
        createdAt: new Date(),
        updatedAt: new Date()
      }
    ], {});
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    await queryInterface.bulkDelete('Teachers', null, {});
  }
};
