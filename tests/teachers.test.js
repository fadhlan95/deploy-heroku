const request = require("supertest");
const app = require("../app");
const { Teacher } = require("../models");
// const { response } = require("express");

describe('GET /teachers', function() {
  it('responds with status code 200', function(done) {
    request(app)
      .get('/teachers')
      .then((response) => {
        let { body, status } = response;
        expect(status).toBe(200);
        expect(body).toHaveProperty('statusCode');
        done();
      })
      .catch(err => {
        done(err)
      })
      
  });

  it('responds with json array of object inside data key', function(done) {
    request(app)
      .get('/teachers')
      .then((response) => {
        let { body, status } = response;
        expect(status).toBe(200);
        expect(body).toHaveProperty('data');
        done();
      })
      .catch(err => {
        done(err)
      })
      
  });
});